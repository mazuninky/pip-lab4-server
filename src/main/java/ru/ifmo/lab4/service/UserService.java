package ru.ifmo.lab4.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ifmo.lab4.data.entity.UserEntity;
import ru.ifmo.lab4.data.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void registerUser(UserEntity entity) {
        userRepository.save(entity);
    }

    public UserEntity getUserByLogin(String login) {
        return userRepository.findByLogin(login);
    }
}
