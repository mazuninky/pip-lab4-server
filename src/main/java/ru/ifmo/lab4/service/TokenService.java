package ru.ifmo.lab4.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;

import java.io.UnsupportedEncodingException;

public class TokenService {
    private String secretKey = "$P)Dd0VyKcd@IhL0k$*M0nJ1ZP0UZamc!7(cn%_bB@zJ3*l*";

    public TokenService() {

    }

    public String generateToken(long id) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            return JWT.create()
                    .withClaim("id", id)
                    .withIssuer("ifmo")
                    .sign(algorithm);
        } catch (UnsupportedEncodingException | JWTCreationException exception) {
            return null;
        }
    }

    public boolean verifyToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("ifmo")
                    .acceptExpiresAt(5000)
                    .build();
            verifier.verify(token);
            return true;
        } catch (UnsupportedEncodingException | JWTVerificationException exception) {
            return false;
        }
    }
}
