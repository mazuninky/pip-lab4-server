package ru.ifmo.lab4.response;

public class PointResponse {
    private final String x;
    private final String y;
    private final String r;
    private final boolean inArea;

    public PointResponse(String x, String y, String r, boolean inArea) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.inArea = inArea;
    }

    public String getX() {
        return x;
    }

    public String getY() {
        return y;
    }

    public String getR() {
        return r;
    }

    public boolean isInArea() {
        return inArea;
    }
}
