package ru.ifmo.lab4.response;

public class UserResponse {
    private final String login;
    private final String password;

    public UserResponse(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
