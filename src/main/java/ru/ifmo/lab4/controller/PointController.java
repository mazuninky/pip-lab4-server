package ru.ifmo.lab4.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ifmo.lab4.domain.PointValidator;
import ru.ifmo.lab4.domain.PointValidatorImpl;
import ru.ifmo.lab4.response.PointResponse;

@RestController
public class PointController {
    private static PointValidator pointValidator = new PointValidatorImpl();

    @RequestMapping(value = "/point", method = RequestMethod.POST)
    public PointResponse checkHit(@RequestParam(value = "x") String x, @RequestParam(value = "y") String y, @RequestParam(value = "r") String r) {
        //validate
        return new PointResponse(x, y, r, true);
    }
}
