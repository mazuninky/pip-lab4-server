package ru.ifmo.lab4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ifmo.lab4.data.entity.UserEntity;
import ru.ifmo.lab4.data.repository.UserRepository;
import ru.ifmo.lab4.response.UserResponse;
import ru.ifmo.lab4.service.UserService;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public UserResponse loginUser(@RequestParam(value = "login") String login, @RequestParam(value = "password") String password) {
        UserEntity user = userService.getUserByLogin(login);
        if(user == null)
            return new UserResponse("", "");
        return new UserResponse(user.getLogin(), user.getPassword());
    }
}
