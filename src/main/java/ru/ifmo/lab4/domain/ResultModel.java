package ru.ifmo.lab4.domain;

import java.math.BigDecimal;

public class ResultModel {
    private final BigDecimal x;
    private final BigDecimal y;
    private final BigDecimal r;
    private final boolean inArea;

    public ResultModel(BigDecimal x, BigDecimal y, BigDecimal r, boolean inArea) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.inArea = inArea;
    }

    public BigDecimal getX() {
        return x;
    }

    public BigDecimal getY() {
        return y;
    }

    public BigDecimal getR() {
        return r;
    }

    public boolean isInArea() {
        return inArea;
    }
}
