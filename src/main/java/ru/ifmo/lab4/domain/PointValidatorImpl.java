package ru.ifmo.lab4.domain;

public class PointValidatorImpl implements PointValidator {
    /**
     * Область задаётся данными условиями:
     * Для 1 : y <= r && x <= r/2
     * Для 3 : y >= -x/2 - 1
     * Для 4 : x^2 + y^2 <= r ^2
     * @param model Точка для проверки
     * @return
     */
    @Override
    public ResultModel validatePoint(PointModel model) {
        return new ResultModel(model.getX(), model.getY(), model.getR(), false);
    }
}
